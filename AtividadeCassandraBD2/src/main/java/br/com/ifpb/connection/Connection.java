package br.com.ifpb.connection;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

/**
 *
 * @author aguirre
 */
public class Connection {
    private static Cluster cluster = null;

    public static Session getConnection() {
        cluster = Cluster.builder().addContactPoint("127.0.0.1").build();

        Session session = cluster.connect("atividade");

        return session;
    }

    public static void closeConnection() {
        cluster.close();
    }
}
