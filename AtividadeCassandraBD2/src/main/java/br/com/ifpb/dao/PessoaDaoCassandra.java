package br.com.ifpb.dao;

import br.com.ifpb.connection.Connection;
import br.com.ifpb.model.Pessoa;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.Result;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aguirre
 */
public class PessoaDaoCassandra {

    public PessoaDaoCassandra() {
    }
    
    public boolean create(Pessoa pessoa) {
        Session session = Connection.getConnection();

        MappingManager mappingManager = new MappingManager(session);
        Mapper<Pessoa> mapper = mappingManager.mapper(Pessoa.class);

        mapper.save(pessoa);

        Connection.closeConnection();
        session.close();

        return true;
    }
    
    public Pessoa read(int idPessoa) {
        Session session = Connection.getConnection();

        MappingManager mappingManager = new MappingManager(session);
        Mapper<Pessoa> mapper = mappingManager.mapper(Pessoa.class);

        Pessoa user = mapper.get(idPessoa);

        Connection.closeConnection();
        session.close();

        return user;
    }
    
    public boolean update(Pessoa usuario) {
        Session session = Connection.getConnection();

        MappingManager mappingManager = new MappingManager(session);
        Mapper<Pessoa> mapper = mappingManager.mapper(Pessoa.class);

        mapper.save(usuario);

        Connection.closeConnection();
        session.close();

        return true;
    }

    public boolean delete(Pessoa usuario) {
        Session session = Connection.getConnection();

        MappingManager mappingManager = new MappingManager(session);
        Mapper<Pessoa> mapper = mappingManager.mapper(Pessoa.class);

        mapper.delete(usuario);

        Connection.closeConnection();
        session.close();

        return true;
    }

    public List<Pessoa> listar() {
        Session session = Connection.getConnection();

        MappingManager mappingManager = new MappingManager(session);
        Mapper<Pessoa> mapper = mappingManager.mapper(Pessoa.class);
        PreparedStatement stmt = session.prepare("SELECT * FROM Pessoa;");

        Result<Pessoa> result = mapper.map(session.execute(stmt.bind()));
        List<Pessoa> usuarios = new ArrayList<>();

        for (Pessoa user: result) {
            usuarios.add(user);
        }

        Connection.closeConnection();
        session.close();

        return usuarios;
    }
}
