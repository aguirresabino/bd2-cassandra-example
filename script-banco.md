CREATE TYPE Telefone(
    numero text
);

CREATE TABLE Pessoa(
    id INT PRIMARY KEY,
    nome TEXT,
    telefones MAP<TEXT, FROZEN <Telefone>>
);